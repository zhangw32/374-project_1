package algorithm.Impl;

import algorithm.Algorithm;
import entity.Elevator;
import entity.Request;

import java.util.List;

public class SATF implements Algorithm {
    @Override
    public List<Request> exec(List<Request> requests, Elevator elevator) {
        requests.sort((r1, r2) -> {
            int r1Cost = Math.abs(r1.getCurrentFloor() - r1.getTargetFloor());
            int r2Cost = Math.abs(r2.getCurrentFloor() - r2.getTargetFloor());
            return r1Cost - r2Cost;
        });
        return requests;
    }
}
