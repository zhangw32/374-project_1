package algorithm.Impl;

import algorithm.Algorithm;
import entity.Elevator;
import entity.Request;

import java.util.ArrayList;
import java.util.List;

public class SSATF implements Algorithm {
    @Override
    public List<Request> exec(List<Request> requests, Elevator elevator) {
        int n = requests.size();
        List<Request> res = new ArrayList<>();

        Integer oldCurrentFloor = elevator.getCurrentFloor();
        for(int i = 0; i < n; i++) {
            int min = requests.get(i).getCurrentFloor();
            int index = i;

            for(int j = i + 1; j < n; j++) {
                Request req = requests.get(j);
                int newCurrentFloor = Math.abs(req.getCurrentFloor() - oldCurrentFloor);
                if(min > newCurrentFloor) {
                    min = newCurrentFloor;
                    index = j;
                }
            }

            Request req = requests.get(index);
            res.add(req);
            oldCurrentFloor = req.getTargetFloor();
        }

        return res;
    }
}
