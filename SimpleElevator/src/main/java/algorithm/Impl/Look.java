package algorithm.Impl;

import algorithm.Algorithm;
import entity.Direction;
import entity.Elevator;
import entity.Request;

import java.util.*;
import java.util.stream.Collectors;

public class Look implements Algorithm {
    @Override
    public List<Request> exec(List<Request> requests, Elevator elevator) {

        Integer direction = elevator.getType();

        // default direction: UP
        if(Direction.EQUALS.getType().equals(direction)) {
            direction = Direction.UP.getType();
        }

        Integer elevatorCurrentFloor = elevator.getCurrentFloor();
        Map<Integer, List<Request>> groupByDirection = requests.stream().collect(
                Collectors.groupingBy(r -> {
                    Integer directionType = Direction.getDirection(r.getCurrentFloor(), r.getTargetFloor()).getType();
                    if(elevatorCurrentFloor.compareTo(r.getCurrentFloor()) > 0) {
                        directionType = Direction.getReverseDirection(directionType);
                    }
                    return directionType;
                }));

        List<Request> res = new ArrayList<>(requests.size());

        List<Request> directionReqList = groupByDirection.get(direction);
        directionReqList.sort(Comparator.comparingInt(Request::getCurrentFloor));

        Integer reverseDirection = Direction.getReverseDirection(direction);
        List<Request> reverseDirectionReqList = groupByDirection.get(reverseDirection);
        reverseDirectionReqList.sort(Comparator.comparingInt(Request::getCurrentFloor));

        res.addAll(directionReqList);

        res.addAll(reverseDirectionReqList);


        return res;
    }
}
