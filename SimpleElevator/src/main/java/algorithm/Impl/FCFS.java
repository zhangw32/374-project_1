package algorithm.Impl;

import algorithm.Algorithm;
import entity.Elevator;
import entity.Request;
import lombok.extern.log4j.Log4j2;

import java.util.Collections;
import java.util.List;

@Log4j2
public class FCFS implements Algorithm {
    @Override
    public List<Request> exec(List<Request> requests, Elevator elevator) {
        requests.sort((r1, r2) -> {
            Long r1StartTime = r1.getDeliveryTime();
            Long r2StartTime = r2.getDeliveryTime();
            if (r1StartTime > r2StartTime) {
                return 1;
            } else if (r1StartTime < r2StartTime) {
                return -1;
            }
            return 0;
        });
        return requests;
    }
}
