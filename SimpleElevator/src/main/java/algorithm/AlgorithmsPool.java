package algorithm;

import algorithm.Impl.*;
import entity.AlgorithmType;

import java.util.HashMap;
import java.util.Map;

public class AlgorithmsPool {
    public static Map<Integer, Algorithm> algorithms = new HashMap<>();

    static {
        algorithms.put(AlgorithmType.FCFS.getType(), new FCFS());
        algorithms.put(AlgorithmType.SSTF.getType(), new SSATF());
        algorithms.put(AlgorithmType.SCAN.getType(), new Scan());
        algorithms.put(AlgorithmType.LOOK.getType(), new Look());
        algorithms.put(AlgorithmType.SATF.getType(), new SATF());
    }
}
