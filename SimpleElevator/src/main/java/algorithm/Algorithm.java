package algorithm;

import entity.Elevator;
import entity.Request;

import java.util.List;

public interface Algorithm {
    List<Request> exec(List<Request> requests, Elevator elevator);
}
