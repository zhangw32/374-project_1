package algorithm;

import entity.AlgorithmType;
import entity.Direction;
import entity.Elevator;
import entity.Request;
import lombok.extern.log4j.Log4j2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Log4j2
public class SimpleAlgorithm {

    public static int exec(List<Request> requests, Elevator elevator) throws InterruptedException {
        log.info("================= Start Scheduling =================");
        log.info("Current Algorithm: {}, Tasks Number: {}, Direction: {}", "Simple", requests.size(), Direction.getDesc(elevator.getType()));

        log.info("Steps:");
        Map<Integer, Integer> pressed = new HashMap<>();
        Map<Integer, Integer> visit = new HashMap<>();

        for (Request t: requests) {
            pressed.put(t.getCurrentFloor(), t.getTargetFloor());
        }

        int distance = 0;
        Direction drt = requests.get(0).getCurrentFloor() > elevator.getCurrentFloor() ? Direction.UP : Direction.DOWN;
        visit.put(requests.get(0).getTargetFloor(), requests.get(0).getCurrentFloor());
        int nextFloor = requests.get(0).getCurrentFloor();
        while (!pressed.isEmpty()) {
            if(drt == Direction.UP) {
                for(int i = elevator.getCurrentFloor(); i <= nextFloor; i++) {
                    Thread.sleep(10L );
                    if(i != elevator.getCurrentFloor()) {
                        distance++;
                    }
                    String logStr = "";
                    logStr = logStr +  "Floor "+ i + "F UP ";
//                    log.info("Floor {}F UP", i);
                    if(pressed.containsKey(i)) {
                        if(pressed.get(i) > nextFloor) {
                            nextFloor = pressed.get(i);
                        }
                        visit.put(pressed.get(i), i);
                        logStr = logStr + "get in: " + i + "->" + pressed.get(i) + " ";
//                        log.info("        Get in: {}->{}", i, pressed.get(i));
                    }

                    if(visit.containsKey(i)) {
                        logStr = logStr + "get out: " + visit.get(i) + "->" + i + " ";
//                        log.info("         Get out: {}->{}", visit.get(i), i);
                        pressed.remove(visit.get(i));
                        visit.remove(i);
                    }
                    log.info(logStr);
                }

                elevator.setCurrentFloor(nextFloor);
                drt = Direction.DOWN;
                // nextFloor = min(visit)
                Set<Integer> keys = pressed.keySet();
                for(Integer k : keys) {
                    if (k < nextFloor) {
                        nextFloor = k;
                    }
                }
            } else {
                for (int i = elevator.getCurrentFloor(); i >= nextFloor; i--) {
//                    distance++;
                    Thread.sleep(10L );
                    if(i != elevator.getCurrentFloor()) {
                        distance++;
                    }


                    String logStr = "";
                    logStr = logStr +  "Floor "+ i + "F DOWN ";
//                    log.info("Floor {}F Down ", i);
                    if (pressed.containsKey(i)) {
                        if(pressed.get(i) < nextFloor) {
                            nextFloor = pressed.get(i);
                        }

                        visit.put(pressed.get(i), i);
                        logStr = logStr + "get in: " + i + "->" + pressed.get(i) + " ";
//                        log.info("      Get in: {}->{}", i, pressed.get(i));
                    }


                    if(visit.containsKey(i)) {
                        logStr = logStr + "get out: " + visit.get(i) + "->" + i + " ";

//                        log.info("       Get out: {}->{}", visit.get(i), i);
                        pressed.remove(visit.get(i));
                        visit.remove(i);
                    }
                    log.info(logStr);
                }
                elevator.setCurrentFloor(nextFloor);
                drt = Direction.UP;
                Set<Integer> keys = pressed.keySet();
                for(Integer k : keys) {
                    if (k > nextFloor) {
                        nextFloor = k;
                    }
                }
            }
        }


//        System.out.println("total distance: " + distance);
//        System.out.printf("average distance: %.2f\n",  (distance / 6.0));
        log.info("================= End Scheduling =================");

        return distance;

    }

}
