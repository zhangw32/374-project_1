package handler;

import algorithm.Algorithm;
import algorithm.AlgorithmsPool;
import entity.*;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Log4j2
public class ElevatorHandler implements Handler{
    @Override
    public List<Result> handler(Elevator elevator, List<Request> requests) {
        boolean check = checkArgs(elevator, requests);
        if(!check) {
            return null;
        }

        Integer algorithmsType = elevator.getAlgorithm();
        Algorithm schedulingAlgorithms = AlgorithmsPool.algorithms.get(algorithmsType);

        log.info("================= Start Scheduling =================");
        log.info("Current Algorithm: {}, Tasks Number: {}, Direction: {}", AlgorithmType.getDesc(elevator.getAlgorithm()), requests.size(), Direction.getDesc(elevator.getType()));
        List<Request> schedulingReqList = schedulingAlgorithms.exec(requests, elevator);
        log.info("================= End Scheduling =================");

        List<Result> resList = new ArrayList<>(schedulingReqList.size());

        for(Request req : schedulingReqList) {
            Result res = new Result();
            res.setRequestTime(req.getDeliveryTime());
            res.setHandlerTime(System.nanoTime());

            Integer receiveCost = receive(elevator, req, res);

            try {
                Thread.sleep(10L * receiveCost);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Integer sendCost = send(elevator, req, res);

            try {
                Thread.sleep(10L * sendCost);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            res.setTimeUnit(TimeUnit.SECONDS);
            res.setDistance(receiveCost + sendCost);
            res.setDoneTime(System.nanoTime());
            resList.add(res);
        }


        return resList;
    }



    protected boolean checkArgs(Elevator elevator, List<Request> requests) {
        Integer maxFloor = elevator.getMaxFloor();
        Integer minFloor = elevator.getMinFloor();
        if(maxFloor <= minFloor) {
            return false;
        }

        Integer currentFloor = elevator.getCurrentFloor();

        if(currentFloor > maxFloor || currentFloor < minFloor) {
            return false;
        }

        Integer algorithmsType = elevator.getAlgorithm();
        if(!AlgorithmsPool.algorithms.containsKey(algorithmsType)) {
            return false;
        }

        return requests != null && !requests.isEmpty();
    }


    protected Integer receive(Elevator elevator, Request req, Result res) {
        Integer oldElevatorCurrentFloor = elevator.getCurrentFloor();
        Integer reqCurrentFloor = req.getCurrentFloor();
        Direction d1 = Direction.getDirection(oldElevatorCurrentFloor, reqCurrentFloor);

        elevator.setCurrentFloor(reqCurrentFloor);
        res.setFromFloor(oldElevatorCurrentFloor);
        log.info("【receive】Current Floor: {}F, Move to: {}F Direction: {}", oldElevatorCurrentFloor, reqCurrentFloor, d1.name());
        return Math.abs(oldElevatorCurrentFloor - reqCurrentFloor);
    }


    protected Integer send(Elevator elevator, Request req, Result res) {
        Integer targetFloor = req.getTargetFloor();
        Integer newElevatorCurrentFloor = elevator.getCurrentFloor();
        Direction d2 = Direction.getDirection(newElevatorCurrentFloor, targetFloor);

        elevator.setCurrentFloor(targetFloor);
        res.setToFloor(newElevatorCurrentFloor);
        log.info("【  send 】Current Floor: {}F, Move to: {}F Direction: {}", newElevatorCurrentFloor, targetFloor, d2.name());
        return Math.abs(newElevatorCurrentFloor - targetFloor);
    }
}
