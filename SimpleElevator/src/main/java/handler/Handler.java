package handler;

import entity.Elevator;
import entity.Request;
import entity.Result;

import java.util.List;

public interface Handler {
    List<Result> handler(Elevator elevator, List<Request> requests);
}
