package entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Statistics {

    long totalCost;

    long avgCost;

    long throughput;

    long avgTurnaroundTime;

    long avgWeightTurnaroundTime;

    long avgWaitTime;

    long avgResponse;
}
