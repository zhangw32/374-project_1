package entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.concurrent.TimeUnit;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Result {

    /**
     * departure floor of this dispatch
     */
    private Integer fromFloor;

    /**
     * destination floor of this dispatch
     */
    private Integer toFloor;

    /**
     * cost distance
     */
    private Integer distance;

    /**
     * time unit
     */
    private TimeUnit timeUnit;

    /**
     * request time
     */
    private Long requestTime;

    /**
     * time of request start processing
     */
    private Long handlerTime;

    /**
     * completion time
     */
    private Long doneTime;
}
