package entity;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public enum AlgorithmType {

    FCFS(0, "FCFS"),
    SSTF(1, "SSTF"),
    SCAN(2, "SCAN"),
    LOOK(3, "LOOK"),
    SATF(4, "SATF");
    private Integer type;
    private String desc;

    public Integer getType() {
        return this.type;
    }

    public static String getDesc(Integer type) {
        AlgorithmType[] values = values();
        for(AlgorithmType value : values) {
            if(value.type.equals(type)) {
                return value.desc;
            }
        }
        return "UNKNOWN";
    }
}
