package entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Elevator {
    /**
     * current floor
     */
    private Integer currentFloor;

    /**
     * max floor
     */
    private Integer maxFloor;

    /**
     * min floor
     */
    private Integer minFloor;

    /**
     * current direction:
     *  1 UP
     *  -1 DOWN
     *  0 EQUALS
     */
    private Integer type = 1;

    /**
     * scheduling algorithm type
     */
    private Integer algorithm;
}