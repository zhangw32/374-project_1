package entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Request {
    /**
     * current floor
     */
    private Integer currentFloor;

    /**
     * target floor
     */
    private Integer targetFloor;

    /**
     * direction type
     * UP or DOWN
     */
    private Integer direct;

    /**
     * Request delivery time
     */
    private Long deliveryTime;

    public Request(Integer currentFloor, Integer targetFloor) {
        this.currentFloor = currentFloor;
        this.targetFloor = targetFloor;
        this.direct = Direction.getDirection(currentFloor, targetFloor).getType();
        this.deliveryTime = System.nanoTime();
    }
}