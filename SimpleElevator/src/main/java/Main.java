import algorithm.SimpleAlgorithm;
import entity.*;
import handler.ElevatorHandler;
import handler.Handler;
import lombok.extern.log4j.Log4j2;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

@Log4j2
public class Main {
    static Integer currentFloor = 1;

    static Integer maxFloor = 20;

    static Integer minFloor = 1;

    static Handler elevatorHandler = new ElevatorHandler();


    public static void main(String[] args) throws InterruptedException {
        System.out.print("1 normal, 2 benchmark, input 1 or 2: ");
        Scanner sc = new Scanner(System.in);
        int flag = sc.nextInt();
        if(flag == 1) {
            normal();
        } else if (flag == 2) {
            benchmark();
        } else {
            System.out.println("invalid input");
        }
    }

    public static void benchmark() throws InterruptedException {
        System.out.print("Please input tasks number N: ");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        System.out.println("Please input N tasks, e.g. 2 17 refers to the task from 2F to 17F");
        List<Request> requests = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int from = sc.nextInt();
            int to = sc.nextInt();
            Request r = new Request(from, to);
            requests.add(r);
        }
        for(int i = 0; i < 5; i++) {
            benchmarkExec(requests, i);
        }
        normalExec(requests);

    }

    public static void benchmarkExec(List<Request> requests, int algorithmType) {

        log.info("-------------Start Algorithm: {}-------------", AlgorithmType.getDesc(algorithmType));

        Elevator elevator = new Elevator(currentFloor, maxFloor, minFloor, Direction.UP.getType(), algorithmType);

        double avgDistance = 0L;
        long totalDistance = 0L;

        long startTime = System.nanoTime();
        List<Result> resList = elevatorHandler.handler(elevator, requests);

        for(Result res : resList) {

            long distance = res.getDistance();

            totalDistance += distance;

//            log.info("res: {}", res);
        }
        long endTime = System.nanoTime();

        avgDistance = totalDistance / (requests.size() * 1.0);

        long throughputMs = TimeUnit.MILLISECONDS.convert((endTime - startTime), TimeUnit.NANOSECONDS) / requests.size();;


        log.info("【Total Distance】{}", totalDistance);
        log.info("【Average Distance】{}", avgDistance);
        log.info("【Average Time】{} ms", throughputMs);

        log.info("-------------End Algorithm: {}-------------", AlgorithmType.getDesc(algorithmType));
    }

    public static void normal() throws InterruptedException {
        System.out.print("Please input tasks number N: ");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        System.out.println("Please input N tasks, e.g. 2 17 refers to the task from 2F to 17F");
        List<Request> requests = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int from = sc.nextInt();
            int to = sc.nextInt();
            Request r = new Request(from, to);
            requests.add(r);
        }

        normalExec(requests);

    }

    public static void normalExec(List<Request> requests) throws InterruptedException {
        log.info("-------------Start Algorithm: {}-------------", "Simple");

        Elevator elevator = new Elevator(currentFloor, maxFloor, minFloor, Direction.UP.getType(), -1);


        long startTime = System.nanoTime();
        int totalDistance = SimpleAlgorithm.exec(requests, elevator);

        long endTime = System.nanoTime();

        double avgDistance = totalDistance / (requests.size() * 1.0);

        long throughputMs = TimeUnit.MILLISECONDS.convert((endTime - startTime), TimeUnit.NANOSECONDS) / requests.size();;


        log.info("【Total Distance】{}", totalDistance);
        log.info("【Average Distance】{}", avgDistance);
        log.info("【Average Time】{} ms", throughputMs);

        log.info("-------------End Algorithm: {}-------------", "Simple");
    }


    
//    static List<Request> randomRequest(Integer maxFloor, Integer minFloor, Integer num) {
//        SecureRandom secureRandom = new SecureRandom();
//        List<Request> requests = new ArrayList<>(num);
//        int bound = maxFloor - minFloor + 1;
//        for(int i = 0; i < num; i++) {
//            Integer currentFloor = secureRandom.nextInt(bound) + minFloor;
//            Integer targetFloor = secureRandom.nextInt(bound) + minFloor;
//            Request req = new Request(currentFloor, targetFloor);
//            requests.add(req);
//        }
//        return requests;
//    }


}

